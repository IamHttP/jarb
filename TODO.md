### TODO (Coming soon!)
[ ] Support multiple entryPoints in webpack (+ docs, + tests for it)\
[ ] use JSdoc programmatically instead of 'npm run..'
[ ] Add tests for buildHooks: onBuildComplete and onBuildStart\
[ ] Fix build:docs in JARB project (fails with error)\
[ ] npm start should dynamically find a port instead of failing\
[ ] Find a solution for webpack-bundle-analzyer(it's currently run as a command in execSync);
[ ] Improve readme example, to show what are the possibilities of JARB (scss, file-loaders etc.)

### DONE/IN PROGRESS (For next release...)
