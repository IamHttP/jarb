QA Steps

[ ] npm run test\
[ ] test a projectConfig with staging and production environments\
[ ] Test component build\
[ ] Test assetsPath changes through projectConfig.env\
[ ] Test isMinified changes through projectConfig.env\
[ ] tools/webpackConfig can be extended\
[ ] Jest config can be extended
[ ] running npm run build -- --autofixLint should fix linting errors