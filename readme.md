### Quick start

* Create package.json in directory

  ```
  {
    "name": "PROJECT_NAME",
    "version": "0.0.1",
    "description": "Example usage of the reusable webpack configuration",
    "scripts" : {
      "init": "npm install && jarb init",
    },
    "devDependencies": {
      "jarb": "*"
    }
  }
  ```
  
* ``` npm run init``` to setup your react environment
* ``` npm start``` to start webpack dev server (the entrypoint is src/develop.js)
* ``` npm run build``` to build for production (the entrypoint is src/index.js)
* ``` npm run tdd``` to run jest in watch mode
* ``` npm run test``` to run jest and provide test coverage
* ``` npm run release``` to release your code, push to a repository and NPM (if configured)

###Change log
## 3.0.9
[V] Fix failing build when tutorials directory does not exist  
[V] Add dynamic port allocation to npm start instead of failing if port is taken
## 3.0.8
[V] FIX - npm run test will now rebuild jestConf correctly
[V] FIX - JARB will now create missing .dll directory if it does not exist in src 
## 3.0.7
[V] Added typescript support (ts-loader as part of the default loaders) 
## 3.0.6
[V] Added stats:minimal to webpack.dev configuration
## 3.0.5
[V] Updated readme to include a proper example
## 3.0.4
[V] Removed the pre-defined babel-loader options in webpack config, deferred all config to .babelrc\
[V] Replaced babel-polyfill with core-js/stable
## 3.0.3
[V] Fixed babelRC to support reg-runtime
## 3.0.2
[V] Fixed a bug where the wrong jarbFlag env was being passed to npm start, fixed with --env=develop
## 3.0.1
[V] Added an option to add `-- autofixLint` to npm run build and npm start (`npm run build -- --autofixLint`) 
## 3.0.0
[V] Update Babel to 7 (Extremely complex)\
[V] Update jest to 24.x\
[V] Remove all process.env usage(Minimal items are used)\
[V] Update ESLint to 5.x
  - Updated babel-eslint to 7.2.3
  
[V] Update release-it to latest (10.2.0)\
[V] Change release it scripts to run with node(Currently run as an execute command)\
[V] Add support to Node 10\
[V] Move assetsPath into envVariable, and stop using projectConfig directly for it\
[V] Remove PORT support from node_env\
[V] Change bin/analyze to run with node instead of exec\
[V] Update Webpack to 4\
  - Installed uglifyPlugin
  - Added optmiziation.minimizer in config
  - Installed/replaced mini-css-plugin (removed extractTextPlugin)
  - Updated eslint-loader to 2.1.2
  - Updated babel-loader to 7.1.5  (8.0.5 requires @babel-core, which will be the next step)
  - Updated file-uploader 3.0.1
  - Updated style-loader to 0.23.1
  - Updated to sass-loader to 7.1.0
  - Updated node-sass to 4.x
  - Updated webpack-dev-server to 3.2.0
  - npm run build works correctly!
  - npm start now working
  
[V] Removed redux/react-redux from dll plugin\
[V] Added support for node 10\
[V] Updated dependencies:
  - Installed/replaced mini-css-plugin (removed extractTextPlugin)
  - Updated eslint-loader to 2.1.2
  - Updated babel-loader to 7.1.5  (8.0.5 requires @babel-core, which will be the next step)
  - Updated file-uploader 3.0.1
  - Updated style-loader to 0.23.1
  - Updated to sass-loader to 7.1.0
  - Updated node-sass to 4.x 
  
[V] Embed "tap" as a test utility instead of the hand-made tests\ 
[V] Removed JEST from unit tests\
[V] Added TAP to replace JEST\
[V] Change all Jest commands to run from node\ 
[V] fix npm install to use native npm install instead of manually installing the dependencies\
[V] Removed "npm run test" from the default build process\
[V] Enable linting on all JARB project\
[V] Change all eslint commands to run from node\
  - Removed all eslint commands, eslint is now run directly from webpack
  
[V] Remove react-redux\
[V] Remove redux\
[V] Remove react-test-renderer\
[V] Remove install-local\
[V] Remove prop-types\
[V] init should add React and React-DOM to the project's package.json\
[V] Update to latest React (16.8.2)
    - npm install react 16.8.2 on core project
    - npm install react-dom 16.8.2 on core project
    - npm install enzyme[ ]adapter[ ]react[ ]16  1.9.1 on core project
    - Create a testSetup file in jarb/lib/testSetup.js
    - Add setupFiles for jest to include this setupFile
    - Install Enzyme v3 (3.8.0)

## 2.0.0
[V] added report.html and stats.json to default .gitignore
[V] Add environment specific variables to be passed to Webpack
(allow doing "npm run build --myEnv" with my own custom configuration)
[VT] Fix assets path for "applications"
[VT] Remove /mocks
[VT] Remove develop.js boilerplate
[VT] Change eslint to .eslintrc
[VT] Move devtool config to baseWebpackConfig
[VT] Remove .raw.js
[V] Move build:dll into the js script for npm start (no need to expose it)
[V] Remove build:dist:raw
[V] Remove live example
[V] Move all code into lib/
[V] Create bins for all commands done through jarb
[V] Remove all "NODE_ENV" from various scripts and files
[V] Node ENV should be configured based on the action you want to take and the env you choose
[V] Move src/.dll somewhere else, preferably in MOCKS
[V] Remove scripts/build.js, setup, devServer
[V] Ensure all tests are working
[V] use JARB bin for npm scripts instead of regular scripts
[V] npm run test should automatically extend jestConfig
[V] Allow projectConfig, per env, to control minification (envVariables.isMinified was added, RAW_FILE support removed)
[V] Add the ability to easily add more build environments
- Done through projectConfig.env['staging'] = {}, any item here, if ran through npm run build -- env=staging will use these configurations
[V] Allow assetsPath to be taken from projectConfig.env instead of projectConfig directly
[V] index.html is always created
[V] Remove projectType from projectConfig
[V] Added stdio to all execSync

## 1.1.0
[V] Add onBuildComplete and onBuildStart (tools/buildHooks.js can export an object with these keys as functions)

## 1.0.9
[V] Added support for entryPoints

## 1.0.8
[V] Update Jest to 23.6.0
[V] Removed default build test, instead we're now building directly on example src
[V] Remove karma, Mocha, Chai, isparta, jQuery, classnames, react-hot-reload
[V] Add "webpack-bundle-analzyer" to Jarb

## 1.0.7
- Fixed a bug that caused the readme.md to constantly be re-written
## 1.0.6
- Added support for a custom html file (For application type projects, file to be used is src/dist.html)
## 1.0.5
- Added support for custom html in the body tag for Applications
- Removed index.raw.js support
## 1.0.4
- Fixed a bug regarding using 'time' as a command when it's not supported
## 1.0.3
- Moved jest back to 20.0.1
- Fixed Pathing for app_index.html to include ./ instead of .
## 1.0.2
- Updated Jest to 22.4.2
- Updated release-it to 7.4.1