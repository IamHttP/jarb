'use strict';
const path = require('path');
const fs = require('fs');

function buildConfig(buildType = 'static') {
  if (fs.existsSync(path.join(__dirname, `buildTypes/${buildType}.js`))) {
    let config = require(path.join(__dirname, `buildTypes/${buildType}.js`));

    let extendablePath = path.join(`${__dirname}/../../../../tools/webpackConfig.js`);

    if (fs.existsSync(extendablePath)) {
      config = require(extendablePath)(config);
    }

    return config;
  } else {
    console.error('ERROR, INVALID BUILD TYPE:', buildType);
    throw 'ERROR';
  }
}



module.exports = buildConfig;
