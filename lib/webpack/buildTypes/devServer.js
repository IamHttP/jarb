let CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
let path = require('path');
let MiniCssExtractPlugin = require('mini-css-extract-plugin');
let fs = require('fs');
let webpack = require('webpack');
let baseWebpackConfig = require('./shared/baseWebpackConfig');
let config = require('../../scripts/util/config');
let {rules} = require('./shared/rules');



let manifest;

if (fs.existsSync(`${process.cwd()}/src/.dll/common-manifest.json`)) {
  manifest = require(`${process.cwd()}/src/.dll/common-manifest.json`);
}  else {
  manifest = {};
}


let webpackConfiguration = Object.assign({}, baseWebpackConfig, {
  mode: 'development',
  entry: ['core-js/stable',
    `webpack-dev-server/client?http://127.0.0.1:${config.port}`,
    'webpack/hot/only-dev-server',
    `${process.cwd()}/src/develop`
  ],
  cache: true,
  output: {
    filename: 'index.js'
  },
  devServer: {
    contentBase: './src/',
    historyApiFallback: true,
    hot: true,
    port: config.port,
    publicPath: config.webpackPublicPath,
    noInfo: false,
    stats: 'minimal'
  },

  devtool: 'inline-source-map',
  plugins: [
    new CaseSensitivePathsPlugin(),
    new webpack.DllReferencePlugin({
      context: path.join(process.cwd(), '/src'),
      manifest : `${process.cwd()}/src/.dll/common-manifest.json`
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: 'bundle.css',
      chunkFilename: '[id].css'
    }),
    new webpack.DefinePlugin({
      'process.env': config.envVariables
    })
  ],
  module: {
    rules: [...rules]
  },
  performance: {
    hints: false
  }
});

module.exports = webpackConfiguration;