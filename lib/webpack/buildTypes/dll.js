let MiniCssExtractPlugin = require('mini-css-extract-plugin');
let path = require('path');
let webpack = require('webpack');
let baseWebpackConfig = require('./shared/baseWebpackConfig');
let envConfig = require('../../scripts/util/config');
let {rules} = require('./shared/rules');

let config = Object.assign({}, baseWebpackConfig, {
  mode: 'development',
  entry: {
    common: [`${process.cwd()}/src/.dll/common.dll.js`]
  },
  cache: true,
  devServer: {
    contentBase: './src/',
    historyApiFallback: true,
    hot: true,
    port: envConfig.port,
    publicPath: envConfig.publicPath,
    noInfo: false
  },
  output: {
    filename: '[name].js',
    path: `${process.cwd()}/src/.dll/`,
    // The name of the global variable which the library's
    // require() function will be assigned to
    library: '[name]'
  },
  plugins: [
    new webpack.DllPlugin({
      path: path.join(process.cwd(), '/src/.dll/', '[name]-manifest.json'),
      name: '[name]',
      context: path.resolve(process.cwd(), 'src')
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: 'bundle.css',
      chunkFilename: '[id].css'
    })
  ],
  module: {
    rules: [...rules]
  },
  performance: {
    hints: false
  }
});

module.exports = config;