let fs = require('fs');
let findRoot = require('find-root');

function include(modulePath) {
  let root = findRoot(modulePath);

  if (fs.existsSync(`${root}/package.json`)) {
    let packageJson = JSON.parse(fs.readFileSync(`${root}/package.json`, 'utf-8'));

    if (packageJson['jsnext:main'] || packageJson.module) {
      return true;
    }
  } else {
    return false;
  }
}

module.exports = include;