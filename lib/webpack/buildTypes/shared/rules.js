let envConfig = require('../../../scripts/util/config');
let MiniCssExtractPlugin = require('mini-css-extract-plugin');
let path = require('path');

module.exports.rules = [
  {
    test: /\.(js|jsx)$/,
    include: require('./include'),
    use: {
      loader: 'babel-loader'
    }
  },
  {
    test: /\.jsx?$/, // both .js and .jsx
    loader: 'eslint-loader',
    include: path.resolve(process.cwd(), 'src'),
    enforce: 'pre',
    options: {
      configFile: '.eslintrc',
      fix : envConfig.envVariables.autofixLint === '1'
    }
  },
  {
    test: /\.tsx?$/,
    use: 'ts-loader',
    exclude: /node_modules/,
  },
  {
    test: /\.(scss|css)$/,
    use: [
      {
        loader: MiniCssExtractPlugin.loader,
        options: {
          // you can specify a publicPath here
          // by default it use publicPath in webpackOptions.output
          publicPath: envConfig.webpackPublicPath
        }
      },
      'css-loader',
      'sass-loader'
    ]
  },
  {
    test: /\.(png|jpg|jpeg|gif|woff|woff2|eot|ttf|mp4|ogg|svg)$/,
    use: [`file-loader?name=[path][name].[ext]&publicPath=${envConfig.webpackPublicPath}`]
  }
];