/**
 * Base Webpack configuration that is extended in other envs
 * @module webpack/config/base
 */

'use strict';
let path = require('path');

module.exports = {
  devtool: 'source-map',
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
    mainFields: ['browser', 'jsnext:main', 'module', 'main']
  }
};
