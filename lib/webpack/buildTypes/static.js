let baseWebpackConfig = require('./shared/baseWebpackConfig');
let config = require('../../scripts/util/config');
let fileName = '[name].js';
let CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
let MiniCssExtractPlugin = require('mini-css-extract-plugin');
let path = require('path');
let webpack = require('webpack');
let {rules} = require('./shared/rules');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
let BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin

let entryFiles = {
  index: `${process.cwd()}/src/index`
};

let webpackConfiguration = Object.assign({}, baseWebpackConfig, {
  mode: 'production',
  entry: entryFiles,
  cache: false,
  output: {
    path: `${process.cwd()}/dist`,
    filename: fileName,
    publicPath: config.webpackPublicPath,
    libraryTarget: 'umd',
    sourceMapFilename: '[file].map'
  },
  plugins: [
    new CaseSensitivePathsPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: 'bundle.css',
      chunkFilename: '[id].css'
    }),
    new webpack.DefinePlugin({
      'process.env': config.envVariables
    })
  ],
  module: {
    rules: [...rules]
  },
  optimization: {
    minimizer :[]
  },
  performance: {
    hints: config.envVariables.NODE_ENV === 'production' ? "warning" : false
  }
});



if (config.envVariables.analyzeBundle === '1') {
  webpackConfiguration.plugins.push(new BundleAnalyzerPlugin({
    analyzerMode: "static",
    reportFilename: "report.html",
  }));
}

if (config.envVariables.isMinified === '1') {
  webpackConfiguration.optimization.minimizer.push(
    new UglifyJsPlugin({
      sourceMap: true,
      cache: true
    })
  );
}

module.exports = webpackConfiguration;