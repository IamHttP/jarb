let fs = require('fs');
let path = require('path');

function copyReadMe(targetDir) {
  if (!fs.existsSync(path.resolve(targetDir, 'readme.md'))) {
    fs.writeFileSync(path.join(targetDir, 'readme.md'), 'Readme file - Required - even if empty.');
  }  else {
  }
}

module.exports = copyReadMe;