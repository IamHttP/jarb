let fs = require('fs');
let path = require('path');



/**
 * Enrich the <code>Installing project</code>'s package.json with various scripts
 * This ensures all projects have the same npm interface
 */
function enrichPackageJson(targetDir) {
  let staticsPath = path.resolve(__dirname, '../../statics');
  let tplPkgJson = fs.readFileSync(path.join(`${staticsPath}/package.json`));
  let tplPackage = JSON.parse(tplPkgJson);
  let trgtPkgJson = fs.readFileSync(path.join(`${targetDir}/package.json`));
  let trgtPackage = JSON.parse(trgtPkgJson);

  let scripts = Object.keys(tplPackage.scripts);
  // create scripts key if does not exist
  if (!trgtPackage.scripts) {
    trgtPackage.scripts = {};
  }
  // set main entry point of package.json
  trgtPackage.main = tplPackage.main;
  trgtPackage['jsnext:main'] = tplPackage['jsnext:main'];
  trgtPackage.module = tplPackage.module;
  scripts.forEach((scriptName) => {
    // double check that our template package json has this key
    if (tplPackage.scripts[scriptName]) {
      trgtPackage.scripts[scriptName] = tplPackage.scripts[scriptName];
    }
  });


  // create dependencies if they don't exist
  let deps = trgtPackage.dependencies || {};

  if (!deps.react) {
    deps.react = '16.8.2';
  }

  if (!deps['react-dom']) {
    deps['react-dom'] = '16.8.2';
  }

  trgtPackage.dependencies = deps;

  fs.writeFileSync(path.join(targetDir, 'package.json'), JSON.stringify(trgtPackage, null, '  '));
}

module.exports = enrichPackageJson;