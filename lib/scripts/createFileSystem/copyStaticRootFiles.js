let fs = require('fs');
let path = require('path');

/**
 * Copy(and overwrite) all files located inside <code>./lib/statics/rootFiles</code> to the root of the <code>Installing project</code>
 * These files include <b>.eslintrc, .editorconfig, .babelrc,.release.json</b>
 */
function copyStaticRootFiles(targetDir) {
  let rootFilesPath = path.resolve(__dirname, '../../statics/rootFiles');

  let files = fs.readdirSync(rootFilesPath);
  files.forEach((rootFile) => {
    let fileSource = path.join(rootFilesPath, rootFile);
    let targetFile = path.join(targetDir, rootFile);
    fs.writeFileSync(targetFile, fs.readFileSync(fileSource));
  });
}

module.exports = copyStaticRootFiles;