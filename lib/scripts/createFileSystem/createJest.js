let fs = require('fs');
let path = require('path');
function createJestConfFile(targetDir) {
  let jestPath = path.resolve(__dirname, '../../statics/jest/jestConfig.json');
  let jestObj = JSON.parse(fs.readFileSync(jestPath, 'utf-8'));

  // check if tools/jestConf.js exists, if it does try to use to mutate the conf
  let extendedPath = path.resolve(targetDir, 'tools/jestConfig.js');

  if (fs.existsSync(extendedPath)) {
    jestObj = require(extendedPath)(jestObj);
  }  else {
  }
  // // write the conf to root.
  let targetFile = path.resolve(targetDir, 'jestConfig.json');
  fs.writeFileSync(targetFile, JSON.stringify(jestObj, null, '  '));
}

module.exports = createJestConfFile;