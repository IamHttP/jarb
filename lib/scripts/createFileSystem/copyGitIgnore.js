let fs = require('fs');
let path = require('path');

function copyGitIgnore(targetDir) {
  let gitIgnorePath = path.join(targetDir, '.gitignore');
  let staticsPath = path.resolve(__dirname, '../../statics');

  if (!fs.existsSync(gitIgnorePath)) {
    fs.writeFileSync(gitIgnorePath, fs.readFileSync(path.join(`${staticsPath}/_.gitignore`)));
  }  else {
  }
}

module.exports = copyGitIgnore;