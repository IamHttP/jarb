let fs = require('fs');
let path = require('path');

// TOOD all these statics path should be ONE variable

function copyJsdocConfig(targetDir) {
  let jsDocsConfPath = path.join(targetDir, 'jsdocs.conf.json');
  let staticsPath = path.resolve(__dirname, '../../statics');

  if (!fs.existsSync(jsDocsConfPath)) {
    fs.writeFileSync(jsDocsConfPath, fs.readFileSync(path.join(`${staticsPath}/jsdocs.conf.json`)));
  }  else {
  }
}

module.exports = copyJsdocConfig;