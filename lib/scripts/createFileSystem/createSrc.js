let fs = require('fs');
let path = require('path');

function createSrc(targetDir) {
  let target  = path.join(targetDir, 'src');
  let staticsPath = path.resolve(__dirname, '../../statics');

  if (!fs.existsSync(target)) {
    fs.mkdirSync(target);
    fs.writeFileSync(path.join(target, 'index.js'), '/* entry point for production build */');
    fs.writeFileSync(path.join(target, 'develop.js'), fs.readFileSync(path.join(`${staticsPath}/develop.js`)));
    fs.writeFileSync(path.join(target, 'index.html'), fs.readFileSync(path.join(`${staticsPath}/index.html`)));

    fs.mkdirSync(`${target}/.dll`);
    fs.writeFileSync(path.join(`${target}/.dll/`, 'common.dll.js'), fs.readFileSync(path.join(`${staticsPath}/common.dll.js`)));
  }  else {
  }
}

module.exports = createSrc;