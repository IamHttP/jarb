let fs = require('fs');
let path = require('path');
function createTutorialsDir(targetDir) {
  let staticsPath = path.resolve(__dirname, '../../statics');
  let target = path.join(targetDir, 'tutorials');
  if (!fs.existsSync(target)) {
    fs.mkdirSync(target);
    fs.writeFileSync(`${target}/.keep`, '');
  }  else {
  }
}

module.exports = createTutorialsDir;