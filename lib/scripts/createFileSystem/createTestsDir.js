let fs = require('fs');
let path = require('path');

function createTestsDir(targetDir) {
  let target = path.join(targetDir, '__tests__');
  if (!fs.existsSync(target)) {
    fs.mkdirSync(target);
    fs.writeFileSync(`${target}/.keep`, '');
  }  else {
  }
}

module.exports = createTestsDir;