let projectConf;
let path = require('path');
let fs = require('fs');
let base = process.cwd();
let jarbLib = `${base}/node_modules/jarb/lib`;
let staticsPath = `${jarbLib}/statics`;
let projectConfigPath = path.join(base, 'projectConfig.json');
process.env.JARB_FLAGS = process.env.JARB_FLAGS || '{}';
let jarbFlags = JSON.parse(process.env.JARB_FLAGS) || {};

try {
  projectConf = JSON.parse(fs.readFileSync(projectConfigPath));
} catch (e) {
  // do nothing
  projectConf = {};
}

let projectEnvVariables = {};
if (projectConf.env && jarbFlags.env) {
  projectEnvVariables = projectConf.env[jarbFlags.env];
}

let envVariables = {
  NODE_ENV: process.env.NODE_ENV,
  isMinified: 1,
  assetsPath : '/',
  analyzeBundle: +!!jarbFlags.analyzeBundle,
  autofixLint: +!!jarbFlags.autofixLint
};

Object.assign(envVariables, projectEnvVariables);

Object.keys(envVariables).forEach((key) => {
  envVariables[key] = JSON.stringify(envVariables[key]);
});

module.exports = {
  staticsPath,
  webpackPublicPath: JSON.parse((envVariables.assetsPath || '/').trim()),
  projectConf,
  port: 8000,
  envVariables
};