const webpack = require('webpack');

let config = require('../webpack/webpackConfig')('static');
let compiler = webpack(config);


compiler.run((err, stats) => {
  if (err) {
    console.log(err);
  }
  if (stats.hasErrors()) {
    console.log(stats.toString({
      chunks: false,  // Makes the build much quieter
      colors: true    // Shows colors in the console
    }));
  }
});