import ReactComp from './components/ReactComp';
import scss from './styles/main.scss';

// underscore tests that node_modules are not accidentally transformed by babel
import _ from 'underscore';
import './images/image.png';