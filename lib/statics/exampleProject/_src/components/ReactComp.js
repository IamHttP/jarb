// underscore tests that node_modules are not accidentally transformed by babel
import _ from 'underscore';
import React from 'react';

class ReactComp extends React.Component {
  render() {
    return (
      <div>
        <span>TestString</span>
      </div>
    );
  };
}

export default ReactComp;