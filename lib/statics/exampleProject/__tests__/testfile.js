// underscore cannot work if it's transpiled, this ensures that node_modules are not transpiled
import _ from 'underscore';

import ReactComp from 'components/ReactComp';
import {mount} from 'enzyme';
import React from 'react';
// just a test to force code coverage
import 'index';

describe('works', () => {
  it('works again', () => {
    mount(<ReactComp></ReactComp>);
  });
});