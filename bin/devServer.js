/* eslint no-console:0 */
'use strict';
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const open = require('open');
const path = require('path');
const fs = require('fs');
let staticsPath = path.resolve(__dirname, '../lib/statics');
const getPort = require('get-port');

// does the DLL dir exist?
let dllPath = `${__dirname}/../../../src/.dll`;
let commonDllPath = `${__dirname}/../../../src/.dll/common.dll.js`;

let hasDLL = fs.existsSync(dllPath);
let hasCommon = fs.existsSync(commonDllPath);

if (!hasDLL) {
  fs.mkdirSync(dllPath);
}

if (!hasCommon) {
  fs.writeFileSync(commonDllPath, fs.readFileSync(path.join(`${staticsPath}/common.dll.js`)));
}

let dllConfig = require('../lib/webpack/webpackConfig')('dll');
let compiler = webpack(dllConfig);




compiler.run((err, stats) => {
  if (stats.hasErrors()) {
    console.log(stats.toString({
      chunks: false,  // Makes the build much quieter
      colors: true    // Shows colors in the console
    }));
  }

  // this is the dev server webpack configuration
  // in short, production is not the right name of the 'env'....
  let config = require(path.join(__dirname, '../lib/webpack/buildTypes/devServer'));

  let extendablePath = path.join(`${__dirname}/../../../tools/` + 'webpackConfig.js');
  if (fs.existsSync(extendablePath)) {
    config = require(extendablePath)(config);
  }

  let host = '0.0.0.0';
  getPort({port: config.devServer.port, host}).then((port) => {
    let compiler = webpack(config);
    let devServer = new WebpackDevServer(compiler, config.devServer);

    devServer.listen(port, host, (err) => {
      if (err) {
        console.log(err);
      }
      console.log(`Listening at localhost:${port}`);
      console.log('Opening your system browser...');
      open(`http://localhost:${port}/`);
    });
  });
});