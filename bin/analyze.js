let execSync = require('child_process').execSync;
let fs = require('fs');

const webpack = require('webpack');

let config = require('../lib/webpack/webpackConfig')('static');
let compiler = webpack(config);

compiler.run((err, stats) => {
  if (err) {
    return console.error(err);
  }

  if (stats.hasErrors()) {
    return console.error(stats.toString("errors-only"));
  }
});