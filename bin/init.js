let createJest = require('../lib/scripts/createFileSystem/createJest');
let copyStaticRootFiles = require('../lib/scripts/createFileSystem/copyStaticRootFiles');
let copyGitIgnore = require('../lib/scripts/createFileSystem/copyGitIgnore');
let createTestsDir = require('../lib/scripts/createFileSystem/createTestsDir');
let createSrc = require('../lib/scripts/createFileSystem/createSrc');
let createTutorialsDir = require('../lib/scripts/createFileSystem/createTutorialsDir');
let enrichPackageJson = require('../lib/scripts/createFileSystem/enrichPackageJson');
let copyReadMe = require('../lib/scripts/createFileSystem/copyReadMe');
let copyJsdocConfig = require('../lib/scripts/createFileSystem/copyJsdocConfig');

function installDependencies() {
  let execSync = require('child_process').execSync;
  execSync('npm install --no-save react react-dom', {stdio: [0, 1, 2]});
}

createJest(process.cwd());
copyStaticRootFiles(process.cwd());
copyGitIgnore(process.cwd());
createTestsDir(process.cwd());
createTutorialsDir(process.cwd());
createSrc(process.cwd());
enrichPackageJson(process.cwd());
copyReadMe(process.cwd());
copyJsdocConfig(process.cwd());
installDependencies();


