const jest = require('jest');
const fs = require('fs');
require('../lib/scripts/createFileSystem/createJest')(process.cwd());
const jestConfig = fs.readFileSync(`${process.cwd()}/jestConfig.json`).toString();

// // Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'test';
process.env.NODE_ENV = 'test';
process.env.PUBLIC_URL = '';

process.on('unhandledRejection', (err) => {
  throw err;
});

// ignore the first argv argument that comes from jarb test
let argv = process.argv.slice(3);

let cleanArgs = argv.filter((item) => {
  return item !== '--';
});

// watch by default, unless we need coverage report
if (cleanArgs.indexOf('--coverage') < 0) {
  cleanArgs.push('--watch');
}

cleanArgs.push(
  '--config',
  jestConfig
);

jest.run(cleanArgs);