let fs = require('fs');
let rimraf = require('rimraf');
let path = require('path');
let execSync = require('child_process').execSync;
let {staticsPath, projectConf} = require('../lib/scripts/util/config');
let hooks = {};

if (fs.existsSync(`${process.cwd()}/tools/buildHooks.js`)) {
  hooks = require(`${process.cwd()}/tools/buildHooks.js`);
}

// TODO node_modules/jarb/lib/scripts/webpackBuild.js should be moved to a BIN

rimraf.sync('./docs');
rimraf.sync('./dist');

// check if tutorials dir exists, if it doesn't skip jsdoc
let build = [
  'if [ -d "./tutorials" ]; then jsdoc -c ./jsdocs.conf.json -u ./tutorials -t ./node_modules/ink-docstrap/template -R readme.md -r ./src -d docs; else exit 0; fi',
  'node node_modules/jarb/lib/scripts/webpackBuild.js',
  'if test -e ./dist/bundle.css; then cssnano ./dist/bundle.css ./dist/bundle.css; else echo \'no css file to minify\'; fi'
];

hooks.onBuildStart && hooks.onBuildStart();

// sync file for all commands to run the build
build.forEach((cmd) => {
  let child = execSync(cmd, {stdio: [0, 1, 2]});
});

console.log('Build done!');
let indexHTML = fs.readFileSync(path.join(`${staticsPath}/app_index.html`), 'utf8');

let bodyInnerHTML = projectConf.bodyHTML || '<div id=\'app\'></div>';

indexHTML = indexHTML.replace('{bodyHTML}', bodyInnerHTML);

fs.writeFileSync('./dist/index.html', indexHTML);

let baseDir = process.cwd();

// A custom file might also be present...
if (fs.existsSync(`${baseDir}/src/dist.html`)) {
  let indexHTML = fs.readFileSync(`${baseDir}/src/dist.html`, 'utf8');
  fs.writeFileSync('./dist/index.html', indexHTML);
}

hooks.onBuildComplete && hooks.onBuildComplete();
