#!/usr/bin/env node
let flags = require('minimist')(process.argv.slice(2));
let command = flags._[0];

if (['release', 'init', 'help', 'build', 'devServer', 'test', 'analyze'].indexOf(command) === -1) {
  console.log('JARB: invalid command!, please select one of... [\'init\',\'analyze\', \'help\', \'build\', \'devServer\', \'test\', \'analyze\']');
}

if (!flags.env) {
  flags.env = 'production';
}

process.env.JARB_FLAGS = JSON.stringify(flags);

if (command === 'init') {
  require('./init');
}

if (command === 'build') {
  if (flags.env === 'production') {
    process.env.REACT_WEBPACK_ENV = 'production';
    process.env.NODE_ENV = 'production';
  } else  if (flags.env === 'staging') {
    process.env.NODE_ENV = 'staging';
  }
  require('./build');
}

if (command === 'devServer') {
  process.env.NODE_ENV = 'develop';
  process.env.REACT_WEBPACK_ENV = 'develop';
  require('./devServer');
}

if (command === 'test') {
  require('./test');
}

if (command === 'release') {
  require('./release');
}

if (command === 'analyze') {
  process.env.NODE_ENV = 'production';
  require('./analyze');
}