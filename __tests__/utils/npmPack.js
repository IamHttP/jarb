let execSync = require('child_process').execSync;
let rimraf = require('rimraf');
let fs = require('fs');

function npmPack() {
  execSync('npm pack');

  let res = fs.readdirSync('.');

  let packageFile = res.find((fileName) => {
    return fileName.indexOf('jarb') >= 0 && fileName.indexOf('.tgz') >= 0;
  });

  if (packageFile) {
    fs.renameSync(packageFile, 'tarball.tgz');
  }
}

function cleanPack() {
  rimraf.sync('./tarball.tgz');
}

module.exports = {
  npmPack,
  cleanPack
};