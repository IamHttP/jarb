let fs = require('fs');
let path = require('path');

let exec = require('child_process').exec;
let execWrap = (command, cb) => {
  let child = exec(command, cb);
  child.stderr.pipe(process.stderr);
  child.stdout.pipe(process.stdout);
};

module.exports = {
  execWrap,
  path,
  fs
};