let expect = require('expect');
let rimraf = require('rimraf');

import {execWrap, fs, path} from './bootstrap';


function runDefaultTests(t) {
  t.test('Test that jestConfig exists and that it was extended correctly', (t) => {
    let jestConfigPath = path.resolve('./jestConfig.json');
    let content = fs.readFileSync(jestConfigPath, 'utf-8');
    let jestObj = JSON.parse(content);

    expect(jestObj.testKey).toBe('success');
    t.end();
  });

  t.test('Test that /src was created successfully', (t) => {
    expect(fs.existsSync(path.resolve('src'))).toBeTruthy();
    t.end();
  });

  t.test('Replace /src and run build/project tests', (t) => {
    fs.renameSync('src', 'originalStaticSRC'); // move the created src away
    fs.renameSync('_src', 'src'); // use the placeholder src instead

    t.tearDown(() => {
      fs.renameSync('src', '_src'); // move the created src away
      fs.renameSync('originalStaticSRC', 'src'); // use the placeholder src instead
    });


    t.test('Testing npm run build', (t) => {
      execWrap('npm run build', (err) => {
        expect(err).toBeFalsy();
        // restore all changes
        t.end();
      });
    });

    t.test('test that npm run test works', (t) => {
      execWrap('npm run test', (err) => {
        expect(err).toBeFalsy();

        expect(fs.existsSync(path.resolve('coverage'))).toBeTruthy();
        t.end();
      });
    });

    t.test('test npm run analyze', (t) => {
      execWrap('npm run analyze', (err) => {
        let reportOK = fs.readFileSync('dist/report.html', 'utf8');

        expect(reportOK).toBeTruthy();
        expect(err).toBeFalsy();
        t.end();
      });
    });

    t.test('Add project config, and rebuild the project', (t) => {
      let projectConfigJson = {
        env : {
          production : {
            assetsPath: '//subdomain.google.co.uk/',
            isMinified : 0
          }
        }
      };

      let assetsPaths = projectConfigJson.env.production.assetsPath;
      fs.writeFileSync('./projectConfig.json', JSON.stringify(projectConfigJson));
      fs.writeFileSync('./src/dist.html', '<html>TESTING</html>');

      t.tearDown(() => {
        rimraf.sync('./projectConfig.json');
        rimraf.sync('./src/dist.html');
      });

      t.test('npm run build with projectConfig', (t) => {
        execWrap('npm run build', (err) => {
          let bundleFile = fs.readFileSync('dist/bundle.css', 'utf8');
          let indexFile = fs.readFileSync('dist/index.js', 'utf8');
          let jsOK = indexFile.indexOf('TestString') >= 0;
          let isImagePathCorrect = indexFile.indexOf('src/images/image.png');
          let isScssImagePathOK = bundleFile.indexOf('src/images/scssImage.png') >= 0;
          let cssOK = bundleFile.indexOf('margin') >= 0;
          let originalFile = fs.readFileSync('src/styles/vars.scss', 'utf8');

          expect(isImagePathCorrect).toBeTruthy();
          expect(isScssImagePathOK).toBeTruthy();

          // ensure the original file has duplicate margin
          expect(originalFile.match(/margin/g).length).toBeGreaterThan(1);

          expect(bundleFile.match(/margin/g).length).toBe(1);
          expect(jsOK).toBeTruthy();
          expect(cssOK).toBeTruthy();

          // extra tests for projectConfig
          // - Do we have ./dist/index.html?
          // - Do we have correct urls in SCSS
          // - do we have correct urls in index.js?
          let hasIndexHtml = fs.readFileSync(path.resolve('./dist/index.html'), 'utf-8').indexOf('TESTING') >= 0;
          let bundleCSS    = fs.readFileSync(path.resolve('./dist/bundle.css'), 'utf-8');
          let indexJS      = fs.readFileSync(path.resolve('./dist/index.js'), 'utf-8');

          let bundleHasBasePath  = bundleCSS.indexOf(`${assetsPaths}src/images/scssImage.png`) >= 0;
          let indexJSHasBasePAth = indexJS.indexOf(`${assetsPaths}src/images/image.png`) >= 0;

          expect(err).toBeFalsy();
          expect(hasIndexHtml).toBeTruthy();
          expect(bundleHasBasePath).toBeTruthy();
          expect(indexJSHasBasePAth).toBeTruthy();
          expect(hasIndexHtml).toBeTruthy();
          t.end();
        });
      });


      t.end();
    });
    t.end();
  });
}

module.exports = runDefaultTests;