import {fs} from '../utils/bootstrap';

let tap = require('tap');
let test = tap.test;
let {execWrap} = require('../utils/bootstrap');

test('run jest tests', (t) => {
  process.chdir('.test');
  fs.renameSync('src', 'originalStaticSRC'); // move the created src away
  fs.renameSync('_src', 'src'); // use the placeholder src instead

  t.tearDown(() => {
    fs.renameSync('src', '_src'); // move the created src away
    fs.renameSync('originalStaticSRC', 'src'); // use the placeholder src instead
    process.chdir('../');
  });

  t.test('running npm run test', (t) => {
    execWrap('npm run test', () => {
      t.end();
    });
  });

  t.end();
});