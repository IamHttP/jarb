let tap = require('tap');
let test = tap.test;
let runDefaultTests = require('../utils/runDefaultTests');

test('run local tests', (t) => {
  t.tearDown(() => {
    process.chdir('../');
  });
  process.chdir('.test');
  runDefaultTests(t);
  t.end();
});