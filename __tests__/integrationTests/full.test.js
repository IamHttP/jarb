import tap, {test} from 'tap';
import expect from 'expect';
import {execWrap, fs, path} from '../utils/bootstrap';
let runDefaultTests = require('../utils/runDefaultTests');
require('../utils/npmPack').npmPack();


test('Tests the full integration process', (t) => {
  t.tearDown(() => {
    process.chdir('../');
  });

  process.chdir('.test');

  t.test('tests that npm run init works correctly', (t) => {
    execWrap('npm run init', (err) => {
      expect(err).toBeFalsy();
      t.end();
    });
  });

  // This is already after init, which is a bit strange to assume order...
  t.test('Running the default subTests', (t) => {
    runDefaultTests(t);
    t.end();
  });

  t.end();
});
