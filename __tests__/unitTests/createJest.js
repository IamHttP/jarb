import path from 'path';
import fs from 'fs';
import tap, {test} from 'tap';
import expect from 'expect';
import createJestConfFile from '../../lib/scripts/createFileSystem/createJest';

const mock = require('mock-fs');

mock({
  tmp: {
  },
  [path.join(process.cwd(), 'lib/statics/jest')] : {
    'jestConfig.json' : '{"foo" :"bar"}'
  }
});



test('Tests that jestConfig.json is created correctly', (t) => {
  t.test('Copies jestConfig.json from lib/statics/jest', (t) => {
    let dir = path.resolve(process.cwd(), 'tmp');

    createJestConfFile(dir);
    let contnets = fs.readFileSync(path.resolve(dir, 'jestConfig.json'));
    expect(JSON.parse(contnets.toString()).foo).toBe('bar');

    t.end();
  });

  mock.restore();
  t.end();
});