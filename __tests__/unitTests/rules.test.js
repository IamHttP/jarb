import path from 'path';
import {test} from 'tap';
import expect from 'expect';
import devConfig from '../../lib/webpack/buildTypes/devServer';
import rules from '../../lib/webpack/buildTypes/shared/rules';

test('Tests the rules', (t) => {
  t.test('Ensure rules are immutable in the config', (t) => {
    expect(devConfig.module.rules).not.toEqual(rules);
    t.end();
  });

  t.end();
});