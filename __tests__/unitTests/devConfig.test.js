import tap, {test} from 'tap';
import expect from 'expect';
import devConfig from '../../lib/webpack/buildTypes/devServer';

let devServer = devConfig.devServer;

test('Test the dev configuration', (t) => {
  t.test('Ensure bundled js name - 0', (t) => {
    expect(devConfig.output.filename).toEqual('index.js');
    t.end();
  });


  t.test('ensure devServer configuration', (t) => {
    expect(devServer.contentBase).toEqual('./src/');
    expect(devServer.port).not.toBeUndefined();
    expect(devConfig.output.filename).toEqual('index.js');
    t.end();
  });

  t.test('ensure correct extensions', (t) => {
    expect(devConfig.resolve.extensions).toEqual(['.tsx', '.ts', '.js', '.jsx']);
    t.end();
  });

  t.test('ensure correct modules resolve', (t) => {
    expect(devConfig.resolve.modules).toBeUndefined();
    t.end();
  });

  t.test('Ensure custom port is passed', (t) => {
    expect(devConfig.devServer.port).toEqual(8000);
    t.end();
  });
  
  t.end();
});
