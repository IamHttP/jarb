import tap, {test} from 'tap';
import expect from 'expect';
import distConfig from '../../lib/webpack/buildTypes/static';
import devConfig from '../../lib/webpack/buildTypes/devServer';

test('Test the dev configuration', (t) => {
  t.test('ensure correct dist directory', (t) => {
    let distPath;
    distPath = distConfig.output.path.replace(process.cwd(), '').replace('/', '');

    // this is a hard dependency, we expect it to be specifically dist.
    // if any component wants to change it, they'll have to change it in their own webpack config override
    expect(distPath).toBe('dist');
    t.end();
  });

  t.test('ensure libraryTarget building', (t) => {
    expect(distConfig.output.libraryTarget).toEqual('umd');
    t.end();
  });

  t.test('ensure bundled js name', (t) => {
    expect(distConfig.output.filename).toEqual('[name].js');
    t.end();
  });

  t.test('ensure correct extensions', (t) => {
    expect(distConfig.resolve.extensions).toEqual(['.tsx', '.ts', '.js', '.jsx']);
    t.end();
  });

  t.test('ensure correct modules resolve', (t) => {
    expect(distConfig.resolve.modules).toBeUndefined();
    t.end();
  });

  t.test('ensure correct plugins', (t) => {
    let requiredPlugins = {
      AggressiveMergingPlugin: 'AggressiveMergingPlugin'
    };
    distConfig.plugins.forEach((item) => {
      delete (requiredPlugins[item.constructor.name]);
    });

    expect(requiredPlugins).toEqual({});
    t.end();
  });

  t.end();
});