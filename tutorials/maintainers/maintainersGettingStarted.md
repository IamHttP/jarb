Requirements: 
Node >6.0.0

### Project purpose
This project attemps to provide the following solution to web development
1. Zero configuration for the developer - extremely fast set up times.
2. Global updates (ability to update all environments using it at once, without conflicts).
3. Unified interface for npm scripts, extremely important for automated tasks that depend on that interface.
4. Seperation of concerns between infrastructure and business logic

### Project directory structure
`__tests__` - Contains the unit tests for the webpack configuration.
`jest` - Contains global `__mocks__` and jestConfig.json, referenced in package.json.
`lib/statics/exampleProject` - Used as boilerplate when running the integration tests.
the first thing we do is copy these contents into the created directory.
`lib/statics/rootFiles` - Contains all the files that are copied to the root of the installing project. - Anything in this directory will be copied forcefully into the installing project's root.
`lib/statics/**` - various other files that are conditionally copied.(while some of them are root files, they are not forcefully copied).
`tutorials` - Contains the various docs tutorials.
`webpack` - Contains the dev server and configuration for various environments.

### Installing the package as a local dependency
Since this package isn't used on it's on, it's often useful to install as a dependency to test modifications.
Since installing a remote dependency requires us to constantly push changes to the remote repo, it is an undesired behaviour.
npm allows us to specifiy a local project.

steps required :
1. Clone this repo into /path/to/project
2. Create a new project in /path/to/project_test
3. Create a package.json inside project_test dir as the {@tutorial developersQuickStart} requires.
4. Rename the dependency path to `file:path/to/project_test` - this path can be relative or absolute.
5. run `npm run init` - this will install the package from your local file system.(note - branch/tag has no meaning in this setting).

### Running tests
Before modifying any files, ensure all tests are running locally.  
After cloning the repo and running `npm install`, run `npm run test:all`.

We run both unit tests and integration tests:
Unit tests are Written inside `__tests__` directory
{@link module:Test_Installation_Process|The integration test is documented here }
 
### Deploying a version
Version numbers use the semver notation of Major.Minor.Patch.
Patch version - Used for internal modifications/documentation that do not affect the end user.
Minor version - Used to add features, all tests must pass as is, new tests are required to be addd for the new features.
Major version - When tests break due to the change, a major version change is required. 
 
### Lifecycle
Below are the steps that take place when installing the package.
1. The repo is defined as a dependency, `npm install` is required.
2. setup.js inside this package is called from the installing project, this enriches the installing project with files and enriches package.json with additional scripts.
You can see the {@link module:Setup_Process|Setup Process} here
3. The webpack configuration is used directly from inside this package in the installing project.

### Editing the documentation
The documentation for files are written inside those files.
The tutorials are written inside /tutorials.
Feel free to contribute to the documentation with a pull request.
Build the docs using `npm run build:docs`